# Hafnium

Hafnium is a hypervisor, initially supporting aarch64 (64-bit Armv8 CPUs).

Development is no longer hosted here, as it has been donated to the
[Trusted Firmware](https://www.trustedfirmware.org/) project hosted by Linaro.
Please see the new
[git repository](https://review.trustedfirmware.org/plugins/gitiles/hafnium/hafnium),
[project page](https://www.trustedfirmware.org/projects/hafnium/) and
[bug dashboard](https://developer.trustedfirmware.org/project/21/item/view/67/)
instead.

This repository is kept for historical reference, but no new changes will be
accepted here.
